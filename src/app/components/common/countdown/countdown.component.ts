import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {map, take} from 'rxjs/operators';
import {timer} from 'rxjs/internal/observable/timer';
import {Subscription} from 'rxjs/internal/Subscription';
import {Vibration} from '@ionic-native/vibration/ngx';
import {Platform} from '@ionic/angular';
import {ExerciseStatusEnum} from '../../../models/exercise-status.enum';

@Component({
    selector: 'app-countdown',
    templateUrl: './countdown.component.html',
    styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy {
    readonly TOTAL_COUNT = 5;      // in seconds

    counter$: Observable<any>;
    counterSub: Subscription;
    remainingTime = this.TOTAL_COUNT;

    @Output()
    done: EventEmitter<null> = new EventEmitter();

    @Output()
    exerciseStatus: EventEmitter<{ status: ExerciseStatusEnum }> = new EventEmitter();

    constructor(private vibration: Vibration,
                private platform: Platform) {
    }

    ngOnInit() {
        this.initObservable();
    }

    startCounter() {
        if (this.counterSub) {
            return;
        }

        this.vibrate();
        this.exerciseStatus.emit({status: ExerciseStatusEnum.Started});

        this.counterSub = this.counter$.subscribe(time => {
            this.remainingTime = time;
        }, null, () => {
            this.vibrate([300, 100, 300]);

            this.remainingTime = this.TOTAL_COUNT;
            this.counterUnsubscribe();

            this.done.emit(null);
        });
    }

    stopCounter() {
        this.counterUnsubscribe();
        this.remainingTime = this.TOTAL_COUNT;

        this.exerciseStatus.emit({status: ExerciseStatusEnum.Stopped});
    }

    ngOnDestroy(): void {
        this.counterUnsubscribe();
    }

    private counterUnsubscribe() {
        if (this.counterSub) {
            this.counterSub.unsubscribe();
            this.counterSub = null;
        }
    }

    private initObservable() {
        this.counter$ = timer(0, 1000).pipe(
            take(this.TOTAL_COUNT + 2),
            map(time => this.TOTAL_COUNT - time)
        );
    }

    private vibrate(time?: number[]) {
        if (this.platform.is('android')) {
            this.vibration.vibrate(time || 300);
        }

        if (this.platform.is('ios')) {
            this.vibration.vibrate(300);
        }
    }
}

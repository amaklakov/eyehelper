import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CountdownComponent} from './countdown.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
    declarations: [CountdownComponent],
    exports: [CountdownComponent],
    imports: [
        CommonModule,
        IonicModule
    ]
})
export class CountdownModule {}

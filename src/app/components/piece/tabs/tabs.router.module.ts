import {NgModule, OnDestroy} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';
import {AuthGuard} from '../../../services/auth.guard';
import {RoutingConst} from '../../../const/routing.const';

const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'tab1',
                canActivate: [AuthGuard],
                children: [
                    {
                        path: '',
                        loadChildren: '../tab1/tab1.module#Tab1PageModule'
                    }
                ]
            },
            {
                path: 'tab2',
                canActivate: [AuthGuard],
                children: [
                    {
                        path: '',
                        loadChildren: '../tab2/tab2.module#Tab2PageModule'
                    }
                ]
            },
            {
                path: 'tab3',
                canActivate: [AuthGuard],
                children: [
                    {
                        path: '',
                        loadChildren: '../tab3/tab3.module#Tab3PageModule'
                    }
                ]
            },
            {
                path: '',
                canActivate: [AuthGuard],
                redirectTo: '/tabs/tab1',
                pathMatch: 'full'
            }
        ]
    },
    {path: RoutingConst.TRAINING.url, loadChildren: '../training/training.module#TrainingPageModule', canActivate: [AuthGuard]},
    {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}

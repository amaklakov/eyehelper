import {Component, OnInit} from '@angular/core';
import {StorageItemsEnum, StorageService} from '../../../services/storage.service';

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
    token;

    constructor(private storage: StorageService) {
    }

    ngOnInit(): void {
        this.getToken();
    }

    getLanguage() {
        return window.navigator.language;
    }

    clearStorage() {
        this.storage.clear();
        this.getToken();
    }

    removeToken() {
       this.storage.setItem(StorageItemsEnum.TOKEN, null);
       this.getToken();
    }

    async getToken() {
        this.token = await this.storage.getItem(StorageItemsEnum.TOKEN);
    }
}

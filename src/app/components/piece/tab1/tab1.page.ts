import {Component, OnInit} from '@angular/core';
import {NewsPieceModel} from './news-piece.model';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
    news: Array<NewsPieceModel>;

    constructor() {
    }

    ngOnInit(): void {
        this.getNews();
    }

    private getNews() {
        this.news = [
            {
                title: 'Публикация изображения',
                subTitle: 'Version 1.0.0',
                description: ` - Все по красоте вообще`,
                date: '06.02.2019',
                image: 'https://www.prismetric.com/wp-content/uploads/blog/mobile_application_development.png'
            },
            {
                title: 'Создание приложения',
                subTitle: 'Version 0.0.2',
                description: ` - Добавлены табы <br>
                               - Первый таб сверстан`,
                date: '06.02.2019',
                image: '/assets/shapes.svg'
            },
            {
                title: 'Идея',
                subTitle: 'Version 0.0.1',
                description: `Идея приложения возникла благодаря тяжелой работе программистов. `
                    + `У нас часто устают глаза после тяжелого рабочего дня...`,
                date: '06.02.2019',
                image: 'https://optinmonster.com/wp-content/uploads/2017/02/Blog-Post-Ideas-1.png'
            }
        ];
    }
}

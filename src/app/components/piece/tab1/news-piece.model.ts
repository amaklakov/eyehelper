export interface NewsPieceModel {
    title: string;
    subTitle: string;
    description: string;
    date: string;
    image: string;
}

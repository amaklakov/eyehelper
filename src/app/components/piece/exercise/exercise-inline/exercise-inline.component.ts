import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {RoutingConst} from '../../../../const/routing.const';
import {ApplicationService} from '../../../../services/application-service';
import {ExerciseFullModel} from '../../../../models/exercise.model';
import {ExerciseStatusEnum} from '../../../../models/exercise-status.enum';

@Component({
    selector: 'app-exercise-inline',
    templateUrl: './exercise-inline.component.html',
    styleUrls: ['./exercise-inline.component.scss']
})
export class ExerciseInlineComponent implements OnInit, OnChanges {
    readonly defaultBackHref = RoutingConst.TAB_EXERCISES.fullUrl;

    @Input()
    exercise: ExerciseFullModel;

    @Input()
    isLast: boolean;

    isTrainingStarted = false;

    @Output()
    nextEvent: EventEmitter<null> = new EventEmitter();

    @Output()
    exerciseStatus: EventEmitter<{ status: ExerciseStatusEnum }> = new EventEmitter();

    isCompleted: boolean;

    constructor(private appService: ApplicationService) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.isCompleted = false;
        this.exerciseStatus.emit({status: ExerciseStatusEnum.Ready});
    }

    ngOnInit() {
    }

    startExercise() {
        this.isTrainingStarted = true;
    }

    stopExercise() {
        this.isTrainingStarted = false;
    }

    showNext() {
        this.isCompleted = true;
    }

    goNext() {
        this.nextEvent.emit(null);
    }

    sendStatus(status: { status: ExerciseStatusEnum }) {
        this.exerciseStatus.emit(status);
    }
}

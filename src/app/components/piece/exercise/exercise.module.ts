import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExerciseComponent} from './exercise.component';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {CountdownModule} from '../../common/countdown/countdown.module';
import { ExerciseInlineComponent } from './exercise-inline/exercise-inline.component';

@NgModule({
    declarations: [ExerciseComponent, ExerciseInlineComponent],
    imports: [
        IonicModule,
        CommonModule,
        TranslateModule,
        CountdownModule
    ],
    exports: [ExerciseComponent, ExerciseInlineComponent]
})
export class ExerciseModule {}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ApplicationService} from '../../../services/application-service';
import {StorageItemsEnum} from '../../../services/storage.service';
import {ExercisesService} from '../../../services/exercises.service';
import {RoutingConst} from '../../../const/routing.const';
import {ExerciseFullModel} from '../../../models/exercise.model';

@Component({
    selector: 'app-exercise',
    templateUrl: './exercise.component.html',
    styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent implements OnInit {
    readonly defaultBackHref = RoutingConst.TAB_EXERCISES.fullUrl;

    exercise: ExerciseFullModel;
    isTrainingStarted = false;

    @Output()
    done: EventEmitter<null> = new EventEmitter();

    constructor(private appService: ApplicationService,
                private exerciseService: ExercisesService) {
    }

    ngOnInit() {
        this.getCurrentExercise();
    }

    startExercise() {
        this.isTrainingStarted = true;
    }

    stopExercise() {
        this.isTrainingStarted = false;
    }

    private async getCurrentExercise() {
        const id = await this.appService.storageService.getItem(StorageItemsEnum.EXERCISE);

        const isCurrentExercise = this.exerciseService.checkExerciseById(id);

        if (isCurrentExercise) {
            this.exercise = this.exerciseService.currentExercise;
            return;
        }

        this.exerciseService.getFullExerciseById(id).subscribe(ex => {
            this.exercise = ex;
        });
    }
}

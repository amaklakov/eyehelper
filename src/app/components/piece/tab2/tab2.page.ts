import {Component, OnInit} from '@angular/core';
import {ApplicationService} from '../../../services/application-service';
import {RoutingConst} from '../../../const/routing.const';
import {ExerciseShortModel} from '../../../models/exercise.model';
import {StorageItemsEnum} from '../../../services/storage.service';
import {ExercisesService} from '../../../services/exercises.service';
import {TrainingService} from '../../../services/training.service';
import {GetTrainingsResponseModel} from '../../../models/get-trainings-response-model';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
    exercises: ExerciseShortModel[] = [];
    trainings: GetTrainingsResponseModel[] = [];

    constructor(private appService: ApplicationService,
                private exercisesService: ExercisesService,
                private trainingService: TrainingService) {
    }

    ngOnInit(): void {
        this.getExercises();
        this.getTrainings();
    }

    async goToExercisePage(id: string) {
        await this.appService.storageService.setItem(StorageItemsEnum.EXERCISE, id);

        // when exercise is loaded => go to page
        this.exercisesService.getFullExerciseById(id).subscribe((res) => {
            this.appService.goToPage(RoutingConst.EXERCISE.fullUrl);
        });
    }

    startTraining() {
        this.appService.goToPage(RoutingConst.TRAINING.fullUrl);
    }

    async startTrainingById(trainingId: string) {
        await this.appService.storageService.setItem(StorageItemsEnum.TRAINING_ID, trainingId);
        this.startTraining();
    }

    private getExercises() {
        this.exercisesService.getExercises().subscribe(res => {
            this.exercises = res;
        });
    }

    /**
     * @deprecated use getExercisesNew() instead
     */
    private getExercisesOld() {
        this.exercisesService.getExercises().subscribe(res => {
            this.exercises = res.reduce((acc, item, index) => {
                if (index % 2 === 0) {
                    acc.push([]);
                }

                acc[Math.floor(index / 2)].push(item);

                return acc;
            }, []);
        });
    }

    private getTrainings() {
        this.trainingService.getTrainings().subscribe(res => {
            this.trainings = res;
        });
    }
}

import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Tab2Page} from './tab2.page';
import {TranslateModule} from '@ngx-translate/core';
import {ExerciseModule} from '../exercise/exercise.module';
import {ExerciseComponent} from '../exercise/exercise.component';
import {RoutingConst} from '../../../const/routing.const';
import {ExercisesService} from '../../../services/exercises.service';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: Tab2Page
            },
            {
                path: RoutingConst.EXERCISE.url,
                component: ExerciseComponent
            }]),
        TranslateModule,
        ExerciseModule
    ],
    declarations: [Tab2Page]
})
export class Tab2PageModule {}

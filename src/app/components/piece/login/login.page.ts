import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidationConst} from '../../../const/validation.const';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../../services/user.service';
import {ApplicationService} from '../../../services/application-service';
import {RoutingConst} from '../../../const/routing.const';
import {ErrorsConst} from '../../../services/errors.const';
import {StorageItemsEnum, StorageService} from '../../../services/storage.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    loginForm: FormGroup;

    constructor(private fb: FormBuilder,
                private translate: TranslateService,
                private loginService: UserService,
                private appService: ApplicationService,
                private storageService: StorageService) {
    }

    ngOnInit() {
        this.storageService.setItem(StorageItemsEnum.TOKEN, null);
        this.initLoginForm();
    }

    async login() {
        if (this.loginForm.invalid) {
            return;
        }

        const {userName, password}: { userName: string, password: string } = this.loginForm.getRawValue();

        this.loginService.getToken(userName, password).subscribe(
            token => {
                this.goToTabsPage();
            }, err => {
                this.loginForm.get('password').reset();

                console.log('err ->', err);
                if (err.error.error === ErrorsConst.INVALID_PASSWORD_OR_EMAIL) {
                    this.appService.alert('MSG.ERROR', 'LOGIN.INVALID_EMAIL_OR_PASSWORD');
                }
            }
        );
    }

    register() {
        this.appService.alert('LOGIN.REGISTER_ALERT_HEADING', 'LOGIN.REGISTER_ALERT_MESSAGE');
    }

    private initLoginForm() {
        this.loginForm = this.fb.group({
            'userName': ['shutilin2101@bsu.by', [Validators.required, Validators.minLength(ValidationConst.USERNAME_MINLENGTH)]],
            'password': ['tg45tg45tg45', [Validators.required, Validators.minLength(ValidationConst.PASSWORD_MINLENGTH)]]
        });
    }

    private goToTabsPage() {
        this.appService.goToPage(RoutingConst.TABS.smallUrl);
    }
}

import {Component, OnInit} from '@angular/core';
import {ApplicationService} from '../../../services/application-service';
import {TrainingService} from '../../../services/training.service';
import {ExerciseFullModel} from '../../../models/exercise.model';
import {RoutingConst} from '../../../const/routing.const';
import {StorageItemsEnum} from '../../../services/storage.service';
import {ExerciseStatusEnum} from '../../../models/exercise-status.enum';

@Component({
    selector: 'app-training',
    templateUrl: './training.page.html',
    styleUrls: ['./training.page.scss']
})
export class TrainingPage implements OnInit {
    readonly defaultBackHref = RoutingConst.TAB_EXERCISES.fullUrl;
    readonly exerciseStatusEnum = ExerciseStatusEnum;

    training: ExerciseFullModel[] = [];
    currentExercise = 0;
    exerciseStatus: ExerciseStatusEnum;

    constructor(private appService: ApplicationService,
                private trainingService: TrainingService) {
    }

    async ngOnInit() {
        const trainingId = await this.appService.storageService.getItem(StorageItemsEnum.TRAINING_ID);

        if (trainingId) {
            return this.trainingService.getTrainingById(trainingId).subscribe(training => {
                this.training = training;
            });
        }

        this.trainingService.getRandomTrainging().subscribe(value => {
            this.training = value;
        });
    }

    skip() {
        this.currentExercise++;
    }

    exit() {
        this.appService.goToPage(RoutingConst.TAB_EXERCISES.fullUrl);
    }

    processStatus(obj: { status: ExerciseStatusEnum }) {
        console.log('here ->', obj);
        this.exerciseStatus = obj.status;
    }
}

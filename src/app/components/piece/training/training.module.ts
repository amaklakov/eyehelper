import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TrainingPage} from './training.page';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http';
import {ExerciseModule} from '../exercise/exercise.module';

const routes: Routes = [
    {
        path: '',
        component: TrainingPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        TranslateModule,
        HttpClientModule,
        ExerciseModule
    ],
    declarations: [TrainingPage]
})
export class TrainingPageModule {
}

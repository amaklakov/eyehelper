import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {RoutingConst} from './const/routing.const';
import {AuthGuard} from './services/auth.guard';

const routes: Routes = [
    {path: '', redirectTo: RoutingConst.LOGIN.fullUrl, pathMatch: 'full'},
    {path: RoutingConst.LOGIN.smallUrl, loadChildren: './components/piece/login/login.module#LoginPageModule'},
    {path: RoutingConst.TABS.smallUrl, loadChildren: './components/piece/tabs/tabs.module#TabsPageModule', canActivate: [AuthGuard]}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}

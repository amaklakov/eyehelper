export class ExerciseShortModel {
    id: string;
    title: string;
    shortDescription?: string;
    image: string;

    constructor(obj?: any) {
        this.id = obj && obj.Id;
        this.title = obj && obj.Name;
        this.shortDescription = obj && obj['Description__c'] || '';
        this.image = obj && obj['Image_URL__c'];
    }
}

export class ExerciseFullModel extends ExerciseShortModel {
    fullDescription: string;

    constructor(obj?: any) {
        super(obj);

        this.fullDescription = obj && obj['Full_Description__c'];
    }

}

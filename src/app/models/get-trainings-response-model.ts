export interface GetTrainingsResponseModel {
    Id: string;
    Name: string;
    Training_Exercise__r: ResponseExercises;
}

export interface ResponseExercises {
    totalSize: number;
    done: boolean;
    records: ResponseExercise[];
}

export interface ExerciseInstance {
    Id: string;
    Name: string;
    Description__c: string;
    Full_Description__c: string;
    Image_URL__c: string;
}

export interface ResponseExercise {
    Training__c: string;
    Id: string;
    Exercise__c: string;
    Exercise__r: ExerciseInstance;
}

export enum ExerciseStatusEnum {
    Ready,
    Started,
    Stopped,
    Done,
}

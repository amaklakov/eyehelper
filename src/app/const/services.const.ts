export class ServicesConst {
    static readonly REAL_BASE = 'https://shutilin-ct-dev-ed.my.salesforce.com';
    static readonly BASE = 'http://134.209.231.82:8080';
    // static readonly BASE = 'https://46.101.239.129:8100';
    // static readonly BASE = 'http://192.168.100.20:8200';
    static readonly API = `services/apexrest/api/exercises`;
    static readonly API_SHORT = `services`;

    static readonly USER_INFO = `${ServicesConst.API}/suserinfo`;
    static readonly GET_EXERCISES = `${ServicesConst.API}/getExercises`;
    static readonly GET_EXERCISE_BY_ID = `${ServicesConst.API}/getExerciseById`;

    static readonly GET_RANDOM_TRAINING = `${ServicesConst.API}/getRandomExercises`;
    static readonly GET_TRAININGS = `${ServicesConst.API}/getTrainings`;
    static readonly GET_TRAINING_BY_ID = `${ServicesConst.API}/getTrainingById`;

    static GET_TOKEN(
        userName = 'shutilin2101@bsu.by',
        password = 'tg45tg45tg45',
        grantType = 'password',
        clientId = '3MVG9oNqAtcJCF.FWpel4an7yvbBMmMXkNVEalvOyTfkpE.SnYqfmGQGbaTSA2DY1b3ls7SGdl3uE6REHAY5P',
        clientSecret = 'F163BF14E19C61EDC4713F9BC67D4D0B3E4AEB91FEFB239DABDD96A912BA9D7F'
    ) {
        return `services/oauth2/token?` +
            `grant_type=${grantType}&` +
            `client_id=${clientId}&` +
            `client_secret=${clientSecret}&` +
            `username=${userName}&` +
            `password=${password}`;
    }
}

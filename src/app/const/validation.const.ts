export class ValidationConst {
    static readonly PASSWORD_MINLENGTH = 6;
    static readonly USERNAME_MINLENGTH = 5;
}

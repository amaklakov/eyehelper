export class RoutingConst {
    static readonly EXERCISE: RoutingItemModel = {
        url: `exercise`,
        fullUrl: `/tabs/tab2/exercise`,
        smallUrl: `/exercise`
    };

    static readonly TRAINING: RoutingItemModel = {
        url: `training`,
        fullUrl: `/tabs/training`,
        smallUrl: `training`
    };

    static readonly TAB_EXERCISES: RoutingItemModel = {
        url: `tab2`,
        fullUrl: `/tabs/tab2`,
        smallUrl: `tab2`
    };

    static readonly TABS: RoutingItemModel = {
        url: `/tabs`,
        fullUrl: `/tabs`,
        smallUrl: `tabs`
    };
    static readonly LOGIN: RoutingItemModel = {
        url: `/login`,
        fullUrl: `/login`,
        smallUrl: `login`
    };
}

export interface RoutingItemModel {
    fullUrl: string;
    smallUrl: string;
    url: string;
}

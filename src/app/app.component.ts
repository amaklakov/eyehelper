import {Component, OnInit} from '@angular/core';
import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private translateService: TranslateService) {
        this.initializeApp();
    }

    ngOnInit(): void {
        this.initTranslateService();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    private initTranslateService() {
        this.translateService.setDefaultLang('ru');

        // if we use browser
        if (window && window.navigator && window.navigator.language) {
            console.log('Language setted as ->', window.navigator.language.split('-')[0]);

            this.translateService.use(window.navigator.language.split('-')[0]);
        }
    }
}

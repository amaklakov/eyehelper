import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServicesConst} from '../const/services.const';
import {map, tap} from 'rxjs/operators';
import {ExerciseFullModel, ExerciseShortModel} from '../models/exercise.model';

@Injectable({
    providedIn: 'root'
})
export class ExercisesService {
    currentExercise: ExerciseFullModel;

    constructor(private httpClient: HttpClient) {
    }

    getExercises() {
        return this.httpClient.post(ServicesConst.GET_EXERCISES, {}).pipe(
            map(res => {
                console.log('res ->', res);
                return (res as any[]).map(item => new ExerciseShortModel(item));
            })
        );
    }

    getFullExerciseById(id: string) {
        return this.httpClient.post(ServicesConst.GET_EXERCISE_BY_ID, {exerciseId: id}).pipe(
            map(res => new ExerciseFullModel(res)),
            tap(exercise => this.currentExercise = exercise)
        );
    }

    checkExerciseById(id: string): boolean {
        if (!this.currentExercise) {
            return false;
        }

        return this.currentExercise.id === id;
    }
}

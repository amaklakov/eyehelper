import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {StorageItemsEnum} from './storage.service';
import {catchError, finalize, switchMap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import {TranslateService} from '@ngx-translate/core';
import {throwError} from 'rxjs/internal/observable/throwError';
import {ServicesConst} from '../const/services.const';
import {ApplicationService} from './application-service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    spinner: any;

    constructor(private appService: ApplicationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log('here', request.url);

        if (!request.url.includes(ServicesConst.API_SHORT)) {
            return this.processRequest(next, request);
        }

        this.showSpinner();
        return fromPromise(this.appService.storageService.getItem(StorageItemsEnum.TOKEN)).pipe(
            switchMap(token => this.makeRequest(token, request, next))
        );
    }

    processRequest(next: HttpHandler, request: HttpRequest<any>) {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                this.showAlert(error);

                if (error.error === 401) {
                    this.appService.storageService.setItem(StorageItemsEnum.TOKEN, null);
                }

                return throwError(error);
            }),
            finalize(() => this.hideSpinner())
        );
    }

    private makeRequest(token, request, next) {
        if (token) {
            token = `OAuth ${token}`;
        }

        const headers = {
            'Authorization': token || '',
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        console.log('====== request ->', request.url);

        request = request.clone({
            setHeaders: {...headers},
            url: `${ServicesConst.BASE}/${request.url}`
        });

        return this.processRequest(next, request);
    }

    private async showSpinner() {
        await this.appService.showSpinner();
    }

    private async showAlert(error: HttpErrorResponse) {
        this.appService.alert(error.error, error.message);
    }

    private hideSpinner() {
        this.appService.hideSpinner();
    }
}

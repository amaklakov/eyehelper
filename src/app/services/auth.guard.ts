import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {StorageItemsEnum, StorageService} from './storage.service';
import {ApplicationService} from './application-service';
import {RoutingConst} from '../const/routing.const';
import {fromPromise} from 'rxjs/internal-compatibility';
import {map, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    private isAuthorized: boolean;

    constructor(private storage: StorageService,
                private appService: ApplicationService) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return fromPromise(this.storage.getItem(StorageItemsEnum.TOKEN)).pipe(
            map(token => token && !!token.length),
            tap(hasToken => {
                // console.log('canActivate token ->', token);

                if (hasToken) {
                    this.isAuthorized = true;
                }

                if (!hasToken) {
                    this.appService.goToPage(RoutingConst.LOGIN.url);

                    if (this.isAuthorized) {
                        this.appService.alert('MSG.TOKEN_ERROR_HEADING', 'MSG.TOKEN_ERROR_BODY');
                        this.isAuthorized = false;
                    }
                }
            })
        );
    }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServicesConst} from '../const/services.const';
import {map, tap} from 'rxjs/operators';
import {StorageItemsEnum, StorageService} from './storage.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private httpClient: HttpClient,
                private storageService: StorageService) {
    }

    getToken(userName: string, password: string) {
        return this.httpClient.post(ServicesConst.GET_TOKEN(userName, password), {}).pipe(
            map(res => res && res['access_token']),
            tap((token: string) => this.storageService.setItem(StorageItemsEnum.TOKEN, token))
        );
    }
}

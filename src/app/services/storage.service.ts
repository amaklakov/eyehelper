import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {
    constructor(private storage: Storage) {
    }

    setItem(name: StorageItemsEnum, value: any) {
        if (!name) {
            throw new Error('No storage item name!');
        }

        return this.storage.set(name, value);
    }

    getItem(name: StorageItemsEnum) {
        return this.storage.get(name);
    }

    async clear() {
        await this.storage.clear();
    }

}

export enum StorageItemsEnum {
    EXERCISE = 'exercise',
    TOKEN = 'Authorization',
    TRAINING_ID = 'trainingId'
}

import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {StorageService} from './storage.service';
import {AlertController, LoadingController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class ApplicationService {
    private spinners = new Set();

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                public storageService: StorageService,
                private alertController: AlertController,
                private spinnerController: LoadingController,
                private translate: TranslateService) {
    }

    getCurrentURL() {
        return this.router.url;
    }

    goToPage(url: string, params?: object) {
        if (url === undefined || url === null) {
            throw new Error(`No url!`);
        }

        if (!params) {
            this.router.navigateByUrl(url);
        }

        if (params) {
            console.log('url, params  ->', url, params);
            const extras: NavigationExtras = {
                queryParams: {
                    state: params
                }
            };

            this.router.navigateByUrl(url, extras);
        }
    }

    getRouteState(parseFunction: (state) => any) {
        this.activatedRoute.queryParams.subscribe(params => {
            console.log('----- ->', params, this.router.getCurrentNavigation());
            const state = this.router.getCurrentNavigation().extras.state;

            if (state) {
                parseFunction(state);
            }
        });
    }

    getParam(paramName: string) {
        return this.activatedRoute.snapshot.paramMap.get(paramName);
    }

    getRoute() {
        return this.activatedRoute;
    }

    async alert(header: string, message: string, buttons?: string[]) {
        const alert = await this.alertController.create({
            header: this.translate.instant(header),
            message: this.translate.instant(message),
            buttons: buttons ? buttons : ['OK']
        });

        await alert.present();
    }

    async showSpinner(msg?: string) {
        const spinner = await this.spinnerController.create({
            showBackdrop: true,
            duration: 5000,
            animated: true,
            message: this.translate.instant(msg ? msg : 'MSG.WAIT')
        });

        spinner.present();

        this.spinners.add(spinner);
    }

    async hideSpinner() {
        this.spinners.forEach(spinner => {
            spinner.dismiss();
        });

        this.spinners.clear();
    }
}

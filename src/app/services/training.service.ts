import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServicesConst} from '../const/services.const';
import {map} from 'rxjs/operators';
import {StorageService} from './storage.service';
import {ExerciseFullModel} from '../models/exercise.model';
import {GetTrainingsResponseModel} from '../models/get-trainings-response-model';

@Injectable({
    providedIn: 'root'
})
export class TrainingService {

    constructor(private http: HttpClient,
                private storeService: StorageService) {
    }

    getRandomTrainging() {
        return this.http.post(ServicesConst.GET_RANDOM_TRAINING, {}).pipe(
            map(res => (res as any[]).map(item => new ExerciseFullModel(item)))
            // tap((value) => this.storeService.setItem(StorageItemsEnum.RANDOM_TRAINING, value))
        );
    }

    getTrainings() {
        return this.http.post<GetTrainingsResponseModel[]>(ServicesConst.GET_TRAININGS, {});
    }

    getTrainingById(trainingId: string) {
        return this.http.post<GetTrainingsResponseModel>(ServicesConst. GET_TRAINING_BY_ID, {trainingId: trainingId}).pipe(
            map(res => res.Training_Exercise__r.records.map(item => new ExerciseFullModel(item.Exercise__r)))
        );
    }
}

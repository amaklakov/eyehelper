# Services

## Get news

**Link** - /*api/getNews*

**Request**

{}

**Response**

```
{
    errorId: number,
    errorMessage: string | null,
    response: {
        title: string;
        subTitle: string;
        description: string;
        date: string;
        image: string;
    }
}
```
